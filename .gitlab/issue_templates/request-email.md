<!-- Purpose of this issue: To suggest a marketing campaign idea to the demand generation team. These will be reviewed and discussed in relation to how they address current and future goals, so please be descriptive in how this idea aligns to KPIs. -->

NOTE: For urgent security and/or support requests, please submit the details below and share the issue link in the [#marketing_programs slack channel](https://gitlab.slack.com/archives/CCWUCP4MS) for immediate visibility.

## Submitter Checklist
* [ ] Name this issue `Email Request: [brief description of email purpose]` (ex. Email Request: help trial users invite new team members)
* [ ] Acknowledge that this is an idea for an email campaign that we will discuss and likely ask questions to better understand the strategy and how it fits within priorities.
* [ ] Review [email best practices](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/#email-marketing-best-practices)
* [ ] Provide logistical details:
   - [ ] **Goal of this email:** `please be as specific as possible, how does this tie into KPIs, and OKRs and link to epics when available`
   - [ ] Requested Send Date: 
   - [ ] Recipient List: (link to salesforce if available)
   - [ ] Reviewers/Approvers:
* [ ] Provide email setup details:
   - [ ] From Name: `ex. GitLab Team`
   - [ ] From Email: `ex. info@gitlab.com`
   - [ ] Subject Line: 
   - [ ] Body Copy (submit below)

Email Body Copy:
`add email body copy here, hyperlink any desired CTAs appropriately`


## Reviewer Checklist
* [ ] Confirm that required details were provided
* [ ] Discuss the proposed send and alignment with priorities, ask questions as necessary
* [ ] Create the email in Marketo
* [ ] Send test email to the requester and designated additional reviewer/approvers
* [ ] Set the email to send on the agreed upon date
* [ ] Confirm the send in the comments of the issue
* Close out this issue.


<!-- DO NOT UPDATE - PROECT MANAGEMENT
/label ~"mktg-status::triage" ~mktg-demandgen ~"dg-campaigns"
/milestone %"DG-Backlog:Requests" 
-->
