<!-- Purpose of this issue: To suggest a webcast idea to the demand generation team. These will be reviewed and discussed in relation to how they address current and future goals, so please be descriptive in how this idea aligns to KPIs. -->

## Submitter Checklist
* [ ] Name this issue `Webcast Idea: [brief descriptive name]` (ex. Webcast Idea: Making the case for CI/CD in your organization)
* [ ] Acknowledge that this is an idea for a webcast that we will discuss and likely ask questions to better understand the strategy and evaluate within priorities.
* [ ] Provide details requested in sections below, for any not filled in, please remove the helper text and DO NOT check the box in the list below.
   - [ ] Basic concept and goals
   - [ ] Potential speakers
   - [ ] Audience
   - [ ] Preliminary research
   - [ ] Timing

#### Basic concept and goals
`Summarize the overall concept of your webcast. What is the goal of the webcast? What will attendees learn? Please share specific KPIs that your idea aims to improve, and ideally share the increase/decrease of that KPI that you are hoping to achieve.`

#### Potential speakers
`Do you have a specific speaker(s) in mind? Have they already expressed interest in participating? Please provide as much detail as possible and links as relevant.`

#### Audience
`Share the function (i.e. security, devops) and seniority (i.e. individual contributors, managers, C-level) as well as any other details you feel are relevant (i.e. users of a specific technology). If there is a geographic region, please indicate as well. Be as specific as possible!`

#### Preliminary research
`Please include any relevant links or commentary as to why this should be a focus in our demand generation horizon.`

#### Timing
`Is there a specific event or timing for which this campaign is time-based? If not, please indicate as such.`



## Reviewer Checklist

* [ ] Discuss in team meeting
* [ ] Share reason for moving forward or closing with idea submitter in comments (and proceed with next steps accordingly)
* Close out this issue.


<!-- DO NOT UPDATE - PROECT MANAGEMENT
/label ~"mktg-status::triage" ~mktg-demandgen ~"dg-campaigns" ~Webcast
/milestone %"DG-Backlog:Ideas" 
-->
