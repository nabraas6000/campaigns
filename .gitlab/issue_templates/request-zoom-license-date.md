<!-- Purpose of this issue: To request reservation of a date for the zoom license for GitLab-hosted virtual events (webcasts and workshops). -->

## Submitter Checklist
* [ ] Name this issue `Zoom License Date Request: [name of virtual event]` (ex. Zoom License Date Request: Mastering CI/CD Webcast)
* [ ] **Acknowledge that all details below must be filled in prior to submitting the request in order to be considered and for the date to be reserved.**
  - [ ] Owner Username: @
  - [ ] Virtual Event Type (select one):
     - [ ] GitLab Hosted Campaign Webcast
     - [ ] GitLab Hosted Partner Webcast
     - [ ] Virtual Workshop
     - [ ] ABM Webcast
  - [ ] Requested Event Date: `YYYY-MM-DD`
  - [ ] Topic: `Add tentative name/topic`
  - [ ] Speakers: 
  - [ ] Target Sales Segment: 
  - [ ] Target Region: 


## Reviewer Checklist
* [ ] Confirm that all requirements above are filled out (if not, move to `~mktg-status::blocked` and comment to the requester)
* [ ] Confirm that date is available (or not) to requester
* [ ] Add to calendar when final date is agreed
* [ ] Comment to requester confirmation that date is secured and on the calendar
* Close out this issue.


<!-- DO NOT UPDATE - PROECT MANAGEMENT
/label ~"mktg-status::triage" ~mktg-demandgen ~"dg-campaigns" ~"Webcast"
/milestone %"DG-Backlog:Requests" 
-->
